## What is Docker?  
![docker](https://d1.awsstatic.com/acs/characters/Logos/Docker-Logo_Horizontel_279x131.b8a5c41e56b77706656d61080f6a0217a3ba356d.png)  
Docker is an open source project that makes it easy to create containers and container-based apps.
## DOCKER TERMINOLOGIES  
### 1.Docker  
A Docker image is a read-only template that contains a set of instructions for creating a container that can run on the Docker platform  
### 2.Docker hub  
Docker Hub is a clustered resource mechanism while working with components of Docker technology. Docker Hub is a best example for public repository is Docker Hub and it helps you in collaborating with your friends to make most from Docker  
### 3.Container?  
A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another.

## Comparing Containers and Virtual Machines  
![container](https://i1.wp.com/www.docker.com/blog/wp-content/uploads/Blog.-Are-containers-..VM-Image-1-1024x435.png?ssl=1)  
### CONTAINERS
Containers are an abstraction at the app layer that packages code and dependencies together. Multiple containers can run on the same machine and share the OS kernel with other containers, each running as isolated processes in user space. Containers take up less space than VMs (container images are typically tens of MBs in size), can handle more applications and require fewer VMs and Operating systems.
### VIRTUAL MACHINES
Virtual machines (VMs) are an abstraction of physical hardware turning one server into many servers. The hypervisor allows multiple VMs to run on a single machine. Each VM includes a full copy of an operating system, the application, necessary binaries and libraries - taking up tens of GBs. VMs can also be slow to boot.  

## BASIC TERMINOLOGIES
![terminology](https://image.slidesharecdn.com/docker-161208023933/95/from-vms-to-containers-introducing-docker-containers-for-linux-and-windows-server-13-638.jpg?cb=1481164794)  
