## What is Git?  
 Git is an Open Source Distributed Version Control System.
 * #### Control System  
 This basically means that Git is a content tracker. So Git can be used to store content — it is mostly used to store code due to the other features it provides  
 * #### Version Control System  
 The code which is stored in Git keeps changing as more code is added. Also, many developers can add code in parallel. So Version Control System helps in handling this by maintaining a history of what changes have happened.  
 * #### Distributed Version Control System  
 Git has a remote repository which is stored in a server and a local repository which is stored in the computer of each developer.Code is not just stored in a central server, but the full copy of the code is present in all the developers’ computers.  

 ## Why Git?  
 * Real life projects generally have multiple developers working in parallel. So a version control system like Git is needed to ensure there are no code conflicts between the developers.  
 * Additionally, the requirements in such projects change often. So a version control system allows developers to revert and go back to an older version of the code.  
 * Sometimes several projects which are being run in parallel involve the same codebase. In such a case, the concept of branching in Git is very important.  
 ## Git Vocabulary  
 * #### Repository  
 Git repository acts as a directory that stores all the files, folders, and content needed for your project.
 * #### Branch   
 A version of the repository that diverges from the main working project. Branches can be a new version of a repository, experimental changes, or personal forks of a repository for users to alter and test changes  
 * #### Clone  
 A clone is a copy of a repository or the action of copying a repository.  
 *  #### Fork  
 Creates a copy of a repository.  
 *  #### Push  
 Updates a remote branch with the commits made to the current branch. You are literally “pushing” your changes onto the remote.  
 *  #### Pull  
 If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request. Pull requests ask the repo maintainers to review the commits made, and then, if acceptable, merge the changes upstream. A pull happens when adding the changes to the master branch.  
 * #### Fetch  
 By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.  
 ![git commands](https://rubygarage.s3.amazonaws.com/uploads/article_image/file/598/git-cheatsheet-4.jpg)    

 ## Git States  
 1)_Modified_ — You have changed files.  
 2)_Staged_ — You have marked a modified file to go into your next commit snapshot.  
 3)_Committed_ — The data is safely stored in your local database.  
 Similarly, based on the states of Git we have corresponding sections in Git.  
 1)_Work Area_  
 2)_Staging Area_  
 3)_Git Repository_    

 ![git pic](https://miro.medium.com/max/700/1*mZV9QPpu6tPC2MaoDUs_Zw.png)  
 The above figure illustrates Git States  
 ## Git Workflow  
 * Clone the repo    
 _$ git clone <link-to-repository>_   
 * Create a new branch    
 _$ git checkout master_    
 _$ git checkout -b <your-branch-name>_    
 * Modify files in your working tree.  
 * Selectively stage just those changes you want to be part of your next commit,
 which adds only those changes to the staging area.  
_$ git add_         # To add untracked files ( . adds all files)   
 * You do a commit, which takes the files as they are in the staging area and stores that
snapshot permanently to your Local Git Repository.  
 _$ git commit -sv_   # Description about the commit  
  

  





 


